import * as bcrypt from 'bcryptjs';
export class Utils {
    public static convertToModel<T1, T2>(Type: { new(p: T1): T2 }, item?: T1): T2 | undefined {
        return item ? new Type(item) : undefined;
    }

    public static PromiseLoop(condition: () => boolean, action: () => Promise<any>): Promise<any> {
        let loop = () => {
            if (condition()) {
                return;
            }
            return Promise.resolve(action()).then(loop).catch(loop);
        };
        return Promise.resolve().then(loop);
    }

}
