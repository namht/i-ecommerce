const path = require('path');
import { Config } from './configs';
const configSystem = new Config();
configSystem.init();
const configDb = configSystem.getPostgresConfig();
module.exports = {
    name: 'default',
    type: 'postgres',
    host: configDb.host,
    port: Number(configDb.port),
    database: configDb.database,
    username: configDb.user,
    password: configDb.password,
    migrationsTableName: 'migration_typeorm',
    entities: [
        path.join(__dirname, '/infrastructure/postgres/typeorm/entities/*{.js,.ts}')
    ],
    migrations: [
        path.join(__dirname, '/infrastructure/postgres/typeorm/migrations/*{.js,.ts}')
    ],
    cli: {
        migrationsDir: '/src/infrastructure/postgres/typeorm/migrations'
    },
    synchronize: true,
};
