import * as fs from 'fs';
import * as path from 'path';
import 'reflect-metadata';
import * as routingController from 'routing-controllers';
import { Container } from 'typedi';
import * as typeorm from 'typeorm';
import '../configs';
routingController.useContainer(Container);
typeorm.useContainer(Container);

export class IOC {
    setup(): any {
        // load lib
        (this.loadModule('../libs'));

        // Load repository module
        (this.loadModule('../infrastructure/postgres/typeorm/repositories'));
        (this.loadModule('../infrastructure/postgres/typeorm/connection'));

        // Load service module
        (this.loadModule('../app/interactors/http_clients'));
        (this.loadModule('../app/interactors'));

        // Load redis module
        (this.loadModule('../infrastructure/redis'));
    }
    private loadModule(dir: string) {
        const folder = path.join(__dirname, dir);
        fs.readdirSync(folder).forEach(file => {
            const isJsFile = file.indexOf('.js') >= 0 ? true : false;
            const isTsFile = file.indexOf('.ts') >= 0 ? true : false;
            const isJsMapFile = file.indexOf('.js.map') >= 0 ? true : false;
            if ((isJsFile || isTsFile) && !isJsMapFile) {
                require(`${folder}/${file}`);
            }
        });
    }
}