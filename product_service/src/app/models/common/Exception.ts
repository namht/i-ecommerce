/**
 * Created by kiettv on 9/29/16.
 */
 import * as trace from 'stack-trace';
 import { HTTP_CODE } from '../../constants';
 
 export class Exception implements Error {
     public name: any;
     public stack: any;
     public message: any;
     public code: number;
     public httpStatus: any;
     public field: string;
     public extension: Exception[];
 
     public static fromError(code: number, error?: Error | null, stack?: boolean, httpStatus?: number): Exception {
         const exception = new Exception(code, error?.message);
         exception.name = 'Exception';
         exception.message = error?.message;
         if (stack && error != null) {
             exception.stack = trace.parse(error);
         }
         if (httpStatus != null) {
             exception.httpStatus = httpStatus;
         }
         else {
             exception.httpStatus = HTTP_CODE.INTERNAL_SERVER_ERROR;
         }
         return exception;
     }
     
 
     constructor(code: number, message?: string, stack?: boolean, httpStatus?: number, field?: string) {
         this.name = 'Exception';
         this.code = code;
         this.message = message;
         if (stack) {
             this.stack = trace.parse(<any> new Error());
         }
         if (httpStatus != null) {
             this.httpStatus = httpStatus;
         }
         else {
             this.httpStatus = HTTP_CODE.INTERNAL_SERVER_ERROR;
         }
 
         if (field) {
             this.field = field;
         }
     }
 
     public toString() {
         return `${this.name}: ${this.message}`;
     }
 }
 
 export default Exception;
 