// import { IUserModel } from "./IUserModel";
// import { IUserManagerModel } from "./IUserManagerModel";
// import { IGetQuotationServiceModel } from "./IGetQuotationServiceModel";
import * as momentTz from "moment-timezone";
export interface ICategoryModel {
    id: string;
    createdDate: momentTz.Moment;
    updatedDate: momentTz.Moment;
    isDeleted: boolean;
    isEnable: boolean;
    name: string;
}