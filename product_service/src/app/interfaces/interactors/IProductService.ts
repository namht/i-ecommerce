import { ProductModel, CollectionWrap } from "../../models";
import { IContext } from "../common/IContext";
export interface IProductService {
    search(ctx: IContext, searchParams: any, offset: number, limit: number, related: string[]): Promise<CollectionWrap<ProductModel>>;
    detail(ctx: IContext, id: string): Promise<ProductModel | undefined>;
    update(ctx: IContext, data: ProductModel): Promise<ProductModel>;
    create(ctx: IContext, data: ProductModel): Promise<ProductModel>;
    delete(ctx: IContext, id: string): Promise<ProductModel>;
}