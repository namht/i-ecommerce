import { ProductModel, CollectionWrap } from '../../models';
import { IBaseRepository } from './IBaseRepository';
import { IContext } from "../common/IContext";

export interface IProductRepository extends IBaseRepository<ProductModel> {
    search(ctx: IContext, searchParams: any, offset: number, limit: number, relate: string[]): Promise<CollectionWrap<ProductModel>>;
}
