// after refactor will separate IBaseRepository to IRead IWrite
export interface IBaseRepository<M> {
    create(data: any): Promise<M>;
    get(id: string): Promise<M | undefined>;
    save(data: M): Promise<M>;
    delete(id: string): Promise<M>;
}
