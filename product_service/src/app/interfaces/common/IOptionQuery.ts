export interface IOptionQuery {
    indexName?: string;
    nextToken?: string;
    limit?: number;
    related?: string[];
}