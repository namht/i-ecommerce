export interface IRelated<M> {
    exec(items: M[]): Promise<M[]>;
}