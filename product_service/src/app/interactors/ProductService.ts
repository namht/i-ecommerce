import Container, { Inject, Service } from 'typedi';
import { IOCServiceName } from '../../ioc/IocServiceName';
import { IProductService } from '../interfaces';
import { Exception as ExceptionModel, ProductModel, CollectionWrap } from '../models';
import { IProductRepository } from '../interfaces/repository';
import { IContext } from "../interfaces/common/IContext";
import { ICategoryHttpClient } from "./http_clients/CategoryClient";
import * as _ from "lodash";
const CategoryClient  = Container.get<ICategoryHttpClient>(IOCServiceName.CATEGORY_CLIENT);
@Service(IOCServiceName.PRODUCT_SERVICE)
class ProductService implements IProductService {
    @Inject(IOCServiceName.PRODUCT_REPOSITORY)
    private readonly ProductRepository: IProductRepository;


    public async search(ctx: IContext, searchParams: any, offset: number, limit: number, related): Promise<CollectionWrap<ProductModel>> {
        let results = await this.ProductRepository.search(ctx, searchParams, offset, limit, related);
        const categoryIds = results.data.filter(item => item.categoryId).map(item => item.categoryId);
        const listCategory = await CategoryClient.findByListIds({}, categoryIds);
        for (let i = 0; i < results.data.length; i++) {
            const product = results.data[i];
            let category = listCategory.filter(item => item.id == product.categoryId);
            if (category && category.length > 0) {
                product.category = category[0];
            }
        }
        return results;
    };
    public async detail(ctx: IContext, id: string): Promise<ProductModel | undefined> {
        return this.ProductRepository.get(id);
    };

    public async update(ctx: IContext, data: ProductModel): Promise<ProductModel> {
        return this.ProductRepository.save(data);
    };

    public async create(ctx: IContext, data: ProductModel): Promise<ProductModel> {
        return this.ProductRepository.create(data);
    };

    public async delete(ctx: IContext, id: string): Promise<ProductModel> {
        return this.ProductRepository.delete(id);
    };
}
export default ProductService;