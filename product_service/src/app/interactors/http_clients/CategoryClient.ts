import { ErrorCode, HTTP_CODE, TIME_ZONE } from '../../constants';
import { Exception, CategoryModel } from '../../models';
import Container, { Inject, Service } from 'typedi';
import { IOCServiceName } from '../../../ioc/IocServiceName';
import { ILogger } from './../../../libs/logger';
import { IConfig } from './../../../configs';

import HttpClient, { IRequestContext } from "./HttpClient";

// Load log and config
const systemConfig = Container.get<IConfig>(IOCServiceName.SYSTEM_CONFIG);
const Logger = Container.get<ILogger>(IOCServiceName.LIB_LOGGER);

const ROUTES = {
    GET_LIST_BY_IDS: '/categories'
};
export interface ICategoryHttpClient {
    findByListIds(context: IRequestContext, ids: string[]): Promise<CategoryModel[]>;
}
@Service(IOCServiceName.CATEGORY_CLIENT)
class CategoryHttpClient extends HttpClient implements ICategoryHttpClient {
    public async findByListIds(context: IRequestContext, ids: string[]): Promise<CategoryModel[]> {
        try {
            const HOST = systemConfig.getMicroServiceHostConfig().category;
            const { data } = await this.send(context, {
                url: HOST + ROUTES.GET_LIST_BY_IDS,
                method: "get",
                params: { ids }
            });

            return data.data;
        } catch (error) {
            Logger.error("[CategoryHttpClient] Failed to findByListIds", error);
            throw error;
        }
    }
}

export default CategoryHttpClient;