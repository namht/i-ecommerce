import * as Axios from 'axios';

const axios = Axios.default;

export interface IRequestContext {
    userId?: string;
}
export class HttpClient {
    public send(context: IRequestContext, options: Axios.AxiosRequestConfig): Promise<any> {
        // TODO: change x-api-key
        options.headers = {
            "x-api-key": "Test API Key",
            ...options.headers,
            "x-user-id": context.userId || "",
        };

        options.timeout = options.timeout || 60000;

        return axios(options);
    }
}

export default HttpClient;
