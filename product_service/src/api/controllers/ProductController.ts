import { Request, Response, urlencoded } from 'express';
import { Get, Post, Put, Delete, JsonController, Req, Res, Body } from 'routing-controllers';
import { Inject, Service } from 'typedi';
import { IProductService } from '../../app/interfaces';
import { IOCServiceName } from '../../ioc/IocServiceName';
import { ErrorCode, HTTP_CODE, TIME_ZONE } from '../../app/constants';
import { Exception, ProductModel } from '../../app/models';

@Service()
@JsonController('/products')
export class ProductController {
    @Inject(IOCServiceName.PRODUCT_SERVICE)
    private ProductService: IProductService;

    @Get('/', { transformResponse: true })
    public async list(@Req() req: Request, @Res() response: Response): Promise<any> {
        let offset = parseInt(req.query.offset as string, 10) || null!;
        let limit = parseInt(req.query.limit as string, 10) || null!;
        let queryParams = req.query || null;

        return await this.ProductService.search({}, queryParams, offset, limit, []);
    }

    @Post('/', { transformResponse: true })
    public async create(@Req() req: Request, @Res() response: Response, @Body() reqBody: any): Promise<any> {
        if (!reqBody.name || !reqBody.price || !reqBody.color || !reqBody.imageUrl || !reqBody.categoryId || !reqBody.branch || !reqBody.description) {
            throw new Exception(
                ErrorCode.RESOURCE.MISSING_REQUIRED_FIELDS.CODE,
                ErrorCode.RESOURCE.MISSING_REQUIRED_FIELDS.MESSAGE,
                false,
                HTTP_CODE.BAD_REQUEST
            );
        }

        let createData = new ProductModel(reqBody);
        createData.isDeleted = false;
        createData.isEnable = true;
        return this.ProductService.create({}, createData);
    }

    @Get('/:id', { transformResponse: true })
    public async get(@Req() req: Request, @Res() response: Response): Promise<any> {
        let { id } = req.params;
        if (!id) {
            throw new Exception(
                ErrorCode.RESOURCE.MISSING_REQUIRED_FIELDS.CODE,
                ErrorCode.RESOURCE.MISSING_REQUIRED_FIELDS.MESSAGE,
                false,
                HTTP_CODE.BAD_REQUEST
            );
        }

        return this.ProductService.detail({}, id);
    }

    @Put('/:id', { transformResponse: true })
    public async update(@Req() req: Request, @Res() response: Response,  @Body() reqBody: any): Promise<any> {
        let { id } = req.params;
        if (!id) {
            throw new Exception(
                ErrorCode.RESOURCE.MISSING_REQUIRED_FIELDS.CODE,
                ErrorCode.RESOURCE.MISSING_REQUIRED_FIELDS.MESSAGE,
                false,
                HTTP_CODE.BAD_REQUEST
            );
        }

        let category = await this.ProductService.detail({}, id);
        if (!category) {
            throw new Exception(
                ErrorCode.RESOURCE.NOT_FOUND.CODE,
                ErrorCode.RESOURCE.NOT_FOUND.MESSAGE,
                false,
                HTTP_CODE.BAD_REQUEST
            );
        }
        if (reqBody.name) {
            category.name = reqBody.name;
        }
        if (reqBody.price) {
            category.price = reqBody.price;
        }
        if (reqBody.color) {
            category.color = reqBody.color;
        }
        if (reqBody.imageUrl) {
            category.imageUrl = reqBody.imageUrl;
        }
        if (reqBody.categoryId) {
            category.categoryId = reqBody.categoryId;
        }
        if (reqBody.branch) {
            category.branch = reqBody.branch;
        }
        if (reqBody.description) {
            category.description = reqBody.description;
        }
        
        return this.ProductService.update({}, category);
    }

    @Delete('/:id', { transformResponse: true })
    public async delete(@Req() req: Request, @Res() response: Response): Promise<any> {
        let { id } = req.params;
        if (!id) {
            throw new Exception(
                ErrorCode.RESOURCE.MISSING_REQUIRED_FIELDS.CODE,
                ErrorCode.RESOURCE.MISSING_REQUIRED_FIELDS.MESSAGE,
                false,
                HTTP_CODE.BAD_REQUEST
            );
        }

        let category = await this.ProductService.detail({}, id);
        if (!category) {
            throw new Exception(
                ErrorCode.RESOURCE.NOT_FOUND.CODE,
                ErrorCode.RESOURCE.NOT_FOUND.MESSAGE,
                false,
                HTTP_CODE.BAD_REQUEST
            );
        }
        category.isDeleted = true;

        return this.ProductService.update({}, category);
    }
}