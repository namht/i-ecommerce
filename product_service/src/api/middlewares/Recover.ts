import * as express from 'express';
import Container from 'typedi';
import { QueryFailedError } from 'typeorm';
import { ErrorCode, HTTP_CODE } from '../../app/constants';
import { Exception } from '../../app/models';
import { IOCServiceName } from '../../ioc/IocServiceName';
import { ILogger } from '../../libs/logger';

interface RecoverFunction {
    (error: any, res: express.Response): void;
}

const recover = (handle?: RecoverFunction): express.ErrorRequestHandler => {
    const fallback: RecoverFunction = (error: any, res: express.Response): void => {
        res.status(500);
        res.end();
    };

    const handler = handle || fallback;
    return (error: any, req: express.Request, res: express.Response, next: express.NextFunction): any => {
        handler(error, res);
        if (!res.writableEnded) {
            fallback(error, res);
        }
    };
};

export function recoverHandler(): express.ErrorRequestHandler {
    const handlerFunction = (error: any, res: express.Response): void => {
        if (!res.headersSent) {
            const logger = Container.get<ILogger>(IOCServiceName.LIB_LOGGER);
            logger.error(error.message ? error.message : 'Unknown error', error);
            let err: Exception;
            if (error.httpStatus != null) {
                err = error as Exception;
            }
            else if ((error instanceof QueryFailedError)) {
                err = new Exception(
                    ErrorCode.RESOURCE.NOT_FOUND.CODE,
                    ErrorCode.RESOURCE.NOT_FOUND.MESSAGE,
                    false,
                    HTTP_CODE.NOT_FOUND,
                );
            }
            else {
                err = Exception.fromError(ErrorCode.UNKNOWN.GENERIC.CODE, error, true);
            }

            res.status(err.httpStatus);
            err.name = undefined;
            err.httpStatus = undefined;
            err.stack = undefined;
            res.json(err);
        }
    };
    return recover(handlerFunction);
}
