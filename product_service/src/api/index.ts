import * as express from 'express';
import { createExpressServer } from 'routing-controllers';
import { json, urlencoded } from 'express';
import * as helmet from 'helmet';
import * as path from 'path';
import * as compression from 'compression';
import { cors } from './middlewares/Cors';
import { notFoundMiddleware, httpError } from './middlewares';
import * as http from 'http';
import { ILogger } from './../libs/logger';
import { recoverHandler } from './middlewares';
export class ApiRouting {
    private logger: ILogger;
    private app: express.Express;
    private port: number;
    private server: http.Server;

    constructor(logger: ILogger) {
        this.logger = logger;
    }

    setup(port: number) {
        this.port = this.normalizePort(port || 3000);
        this.app = createExpressServer({
            // cors: {
            //     origin: '*',
            //     methods: ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'],
            //     allowedHeaders: ['Origin', 'Content-Type', 'Accept', 'Authorization'],
            //     maxAge: 3600,
            //     preflightContinue: true,
            //     optionsSuccessStatus: 204
            // },
            // routePrefix: '/',
            controllers: [
                path.join(__dirname, './controllers/*{.js,.ts}')
            ],
            // middlewares: [
            //     path.join(__dirname, './middlewares/*{.js,.ts}')
            // ],
            middlewares: [
                notFoundMiddleware
            ],
            interceptors: [
                path.join(__dirname, './interceptors/*{.js,.ts}')
            ],
            defaultErrorHandler: false
        });


        this.app.locals.title = 'iCondo Notification';
        this.app.enable('case sensitive routing');
        this.app.enable('trust proxy');
        this.app.disable('x-powered-by');
        this.app.disable('etag');
        this.app.use(json({ limit: '5mb' }));
        this.app.use(urlencoded({ limit: '5mb', extended: false }));
        this.app.use(helmet());
        this.app.use(compression());
        this.app.use(cors());
        this.app.use(httpError(this.logger));
        this.app.use(recoverHandler());
        this.listen(this.port);
    }

    private async listen(port: number) {
        try {
            this.app.set('port', port);
            this.server = http.createServer(this.app);
            this.server.on('error', this.onError.bind(this));
            this.server.on('listening', this.onListening.bind(this));
            this.server.listen(port);
            return this.app;
        }
        catch (err) {
            this.logger.error(err.message, err);
        }
    }

    public getExpressInstance(): express.Express {
        return this.app;
    }

    private onListening() {
        const addr = this.server.address();
        const bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr?.port;
        this.logger.info('Listening on ' + bind);
    }

    private onError(error: any) {
        if (error.syscall !== 'listen') {
            throw error;
        }

        // handle specific listen errors with friendly messages
        switch (error.code) {
        case 'EACCES':
            this.logger.error(this.port + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            this.logger.error(this.port + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
        }
    }

    private  normalizePort(val: any) {
        const port = parseInt(val, 10);
        if (isNaN(port)) {
            return val;
        }
        if (port >= 0) {
            return port;
        }
        return false;
    }
}
