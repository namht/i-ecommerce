import {MigrationInterface, QueryRunner} from "typeorm";

export class initData1625215431037 implements MigrationInterface {
    name = 'initData1625215431037'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
        INSERT INTO "public"."products" VALUES ('b6b17e7c-4414-48a8-813f-9e46cd7b344f', '2021-07-02 09:42:48.083417+00', '2021-07-02 09:42:48.083417+00', 'Rice', 7, 'white', 'https://www.world-grain.com/ext/resources/Article-Images/2020/12/GMR_Rice_AdobeStock_64819529_E_Nov.jpg?1609338918', '', '64584ae5-a58a-480d-a23f-c172e15a6d92', '', 'f', 't');
        INSERT INTO "public"."products" VALUES ('8230850f-69b0-4c96-81b4-49b9d02d5ab0', '2021-07-02 09:43:45.336834+00', '2021-07-02 09:43:45.336834+00', 'Coffee', 2, 'black', 'https://cdn.pixabay.com/photo/2018/11/29/11/50/coffe-3845598_1280.jpg', '', '0c40d69c-aac6-42e9-9a15-643bb8721c7f', '', 'f', 't');
        INSERT INTO "public"."products" VALUES ('c18ecb25-39b4-423a-8536-3f0204b2a64f', '2021-07-02 09:44:07.646895+00', '2021-07-02 09:44:07.646895+00', 'Iced tea', 3, 'yellow', 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/delish-210419-iced-tea-02-landscape-jg-1619020612.jpg?crop=1.00xw:1.00xh;0,0&resize=640:*', '', '0c40d69c-aac6-42e9-9a15-643bb8721c7f', '', 'f', 't');
        INSERT INTO "public"."products" VALUES ('ccca418b-6cf8-4b76-ac91-96fb154d6818', '2021-07-02 09:44:07.048628+00', '2021-07-02 09:44:07.048628+00', 'Water', 3, 'white', 'https://www.cityofpriorlake.com/home/showpublishedimage/1078/637384528077200000', '', '0c40d69c-aac6-42e9-9a15-643bb8721c7f', '', 'f', 't');
        INSERT INTO "public"."products" VALUES ('dd305044-525b-4bfb-863f-e9288226af42', '2021-07-02 09:45:16.697293+00', '2021-07-02 09:45:16.697293+00', 'Bikini', 4, 'yellow', 'https://salt.tikicdn.com/cache/w444/ts/product/ca/e4/6e/72f070cfbde532f1fd95fbec6211936c.jpg', '', '4a39a840-2072-44b6-89c4-04f8b7d67f3d', '', 'f', 't');
        INSERT INTO "public"."products" VALUES ('3251155f-976a-4b89-828f-a279c21ff537', '2021-07-02 09:45:39.962398+00', '2021-07-02 09:45:39.962398+00', 'Skirt', 6, 'orange', 'https://assets.burberry.com/is/image/Burberryltd/d6e2f775f98b3f5d48b3bca7ccb3f3ad00906450.jpg?$BBY_V2_SL_1x1$&wid=2500&hei=2500', '', '4a39a840-2072-44b6-89c4-04f8b7d67f3d', '', 'f', 't');
        INSERT INTO "public"."products" VALUES ('106ff066-1851-4f98-91a7-e5389424f66e', '2021-07-02 09:12:41.469421+00', '2021-07-08 16:20:37.275363+00', 'Humberger', 10, 'red', 'https://chomienque.com/library/uploads/2020/10/Hamburger-bo-xay-chien-scaled.jpg', '', '64584ae5-a58a-480d-a23f-c172e15a6d92', '', 'f', 't');
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }
}
