export const PRODUCT_TABLE_SCHEMA = {
    TABLE_NAME: "products",
    FIELDS: {
        ID: "id",
        IS_ENABLE: "is_enable",
        IS_DELETED: "is_deleted",
        CREATED_DATE: "created_date",
        UPDATED_DATE: "updated_date",
        NAME: "name",
        PRICE: "price",
        COLOR: "color",
        IMAGE_URL: "image_url",
        BRANCH: "branch",
        DESCRIPTION: "description",
        CATEGORY_ID: "category_id"
    }
};