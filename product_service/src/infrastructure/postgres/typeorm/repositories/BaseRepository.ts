import { BaseEntity, Repository, SelectQueryBuilder } from 'typeorm';
import { Converter } from '../converter';
import { isFunction } from 'lodash';
import { CollectionWrap } from '../../../../app/models';
import { BASE_TABLE_SCHEMA } from '../schemas';
import * as uuid from "uuid";
abstract class BaseRepository < D extends BaseEntity, M > {
    protected readonly abstract repository : Repository<D>;
    protected readonly abstract modelType: (new () => M);

    protected readonly abstract TABLE_SCHEMA: {
        TABLE_NAME: string;
        FIELDS: object;
    };
    public async create(data: M): Promise<M> {
        if (!data[BASE_TABLE_SCHEMA.FIELDS.ID]) data[BASE_TABLE_SCHEMA.FIELDS.ID] = uuid.v4();
        const dto = this.repository.create(data);
        await dto.save();
        const insertData = Converter.toModel(this.modelType, dto);
        if (!insertData) {
            throw new Error('error');
        }
        return insertData;
    }
    public async save(data: M): Promise<M> {
        return await this.repository.save(data);
    }

    public async bulkInsert(data: M[]): Promise<boolean> {
        await this.repository.createQueryBuilder(this.TABLE_SCHEMA.TABLE_NAME)
            .insert()
            .values(data)
            .execute();
        return true;
    }
    
    public async delete(id: string): Promise<M> {
        const dto = await this.repository.findOne(id);
        if (dto) {
            await this.repository.remove(dto);
        }
        return Converter.toModel(this.modelType, dto);
    }

    public async get(id: string, relations?: string[]): Promise<M | undefined> {
        const dto = await this.repository.findOne(id, { relations: relations });
        return Converter.toModel(this.modelType, dto);
    }

    public async getOneByConditions(conditions: {}, relations?: string[]): Promise<M | undefined> {
        const dto = await this.repository.findOne(conditions, { relations: relations });
        return Converter.toModel(this.modelType, dto);
    }

    protected async findAllByQueryBuilder(callback: (query: SelectQueryBuilder<D>) => void, aliasName?: string): Promise <M[]> {
        const query = aliasName ? this.repository.createQueryBuilder(aliasName) : this.repository.createQueryBuilder(this.TABLE_SCHEMA.TABLE_NAME);
        callback(query);
        const dto = await query.getMany();
        return dto.map(item => Converter.toModel(this.modelType, item));
    }
    protected async deleteByQueryBuilder(callback: (query: SelectQueryBuilder<D>) => void): Promise <boolean> {
        let query:any = this.repository.createQueryBuilder(this.TABLE_SCHEMA.TABLE_NAME);
        query = query.delete();
        callback(query);
        await query.execute();
        return true;
    }
    protected async findOneByQueryBuilder(query: SelectQueryBuilder<D>): Promise<M | undefined> {
        const dto = await query.getOne();
        return dto && Converter.toModel(this.modelType, dto);
    }
    protected async countByQuery(callback?: (query: SelectQueryBuilder<D>) => void): Promise <number> {
        const query = this.repository.createQueryBuilder(this.TABLE_SCHEMA.TABLE_NAME);
        if (isFunction(callback)) {
            callback(query);
        }
        return await query.getCount();
    }
    protected async searchByQuery(callback?: (query: SelectQueryBuilder<D>) => void): Promise <M[]> {
        const query = this.repository.createQueryBuilder(this.TABLE_SCHEMA.TABLE_NAME);
        if (isFunction(callback)) {
            callback(query);
        }
        // query.offset(skip);
        // query.limit(limit);
        // if (orderBy) {
        //     query.orderBy(orderBy);
        // }
        const dto = await query.getMany();
        return dto.map(item => Converter.toModel(this.modelType, item));
    }
    protected async countAndQuery(countCallback?: (query: SelectQueryBuilder<D>) => void, dataCallback?: (query: SelectQueryBuilder<D>) => void): Promise <CollectionWrap<M>> {
        const count = await this.countByQuery(countCallback);
        const data = await this.searchByQuery(dataCallback);
        const result = new CollectionWrap<M>();
        result.total = count;
        result.data = data;
        return result;
    }
    public deleteCache(cacheKeys: string[]): Promise<any> {
        return Promise.resolve(this.repository.manager.connection.queryResultCache?.remove(cacheKeys));
    }
}

export default BaseRepository;
