import { IProductRepository } from 'src/app/interfaces/repository';
import { Service } from 'typedi';
import { getRepository } from 'typeorm';
import { ProductModel, CollectionWrap } from '../../../../app/models';
import { IOCServiceName } from '../../../../ioc/IocServiceName';
import { ProductEntity } from '../entities/ProductEntity';
import { PRODUCT_TABLE_SCHEMA } from '../schemas';
import BaseRepository from './BaseRepository';
import { IContext } from "../../../../app/interfaces/common/IContext";
import { CONNECTION_NAME } from '../connection';
@Service(IOCServiceName.PRODUCT_REPOSITORY)
export class ProductRepository extends BaseRepository<ProductEntity, ProductModel> implements IProductRepository {
    protected readonly repository = getRepository(ProductEntity, CONNECTION_NAME);
    protected readonly modelType = ProductModel;
    protected readonly TABLE_SCHEMA = PRODUCT_TABLE_SCHEMA;

    public search(ctx: IContext, searchParams: any = {}, offset: number, limit: number, related): Promise<CollectionWrap<ProductModel>> {
        let categoryId = searchParams.categoryId || null;
        limit = limit || 0;
        offset = offset || 0;
        let keyword = searchParams.key || null;
        let fromPrice = searchParams.fromPrice || null;
        let toPrice = searchParams.toPrice || null;
        let color = searchParams.color || null;
        let branch = searchParams.branch || null;
        let sortType = searchParams.sortType || "ASC";

        let query = (offset?: number, limit?: number, isOrderBy?: Boolean) => {
            return (q) => {
                q.where(`${PRODUCT_TABLE_SCHEMA.TABLE_NAME}.${PRODUCT_TABLE_SCHEMA.FIELDS.IS_DELETED} = :isDeleted`, { isDeleted: false });
                q.andWhere(`${PRODUCT_TABLE_SCHEMA.TABLE_NAME}.${PRODUCT_TABLE_SCHEMA.FIELDS.IS_ENABLE} = :isEnable`, { isEnable: true });
                if (categoryId != null) {
                    q.andWhere(`${PRODUCT_TABLE_SCHEMA.TABLE_NAME}.${PRODUCT_TABLE_SCHEMA.FIELDS.CATEGORY_ID} = :categoryId`, { categoryId });
                }
                if (branch != null) {
                    q.andWhere(`${PRODUCT_TABLE_SCHEMA.TABLE_NAME}.${PRODUCT_TABLE_SCHEMA.FIELDS.BRANCH} = :branch`, { branch });
                }
                if (color != null) {
                    q.andWhere(`${PRODUCT_TABLE_SCHEMA.TABLE_NAME}.${PRODUCT_TABLE_SCHEMA.FIELDS.COLOR} = :color`, { color });
                }
                if (fromPrice != null) {
                    q.andWhere(`${PRODUCT_TABLE_SCHEMA.TABLE_NAME}.${PRODUCT_TABLE_SCHEMA.FIELDS.PRICE} >= :fromPrice`, { fromPrice });
                }
                if (toPrice != null) {
                    q.andWhere(`${PRODUCT_TABLE_SCHEMA.TABLE_NAME}.${PRODUCT_TABLE_SCHEMA.FIELDS.PRICE} <= :toPrice`, { toPrice });
                }
                if (keyword != null) {
                    q.andWhere(`${PRODUCT_TABLE_SCHEMA.TABLE_NAME}.${PRODUCT_TABLE_SCHEMA.FIELDS.NAME} ILIKE :keyword`, { keyword: `%${keyword}%` });
                }

                if (offset != null) {
                    q.skip(offset);
                }
                if (limit != null) {
                    q.take(limit);
                }
                if (isOrderBy != null) {
                    q.orderBy(`${PRODUCT_TABLE_SCHEMA.TABLE_NAME}.${PRODUCT_TABLE_SCHEMA.FIELDS.CREATED_DATE}`, sortType);
                }
            };
        };
        return this.countAndQuery(query(), query(offset, limit, true));
    }
}
