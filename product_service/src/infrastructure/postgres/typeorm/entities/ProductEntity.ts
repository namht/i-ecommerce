import { IProductModel } from 'src/app/interfaces/models';
import { Column, Entity } from 'typeorm';
import { PRODUCT_TABLE_SCHEMA } from '../schemas';
import { BaseEntity } from './common/BaseEntity';

@Entity(PRODUCT_TABLE_SCHEMA.TABLE_NAME)
export class ProductEntity extends BaseEntity implements IProductModel {
    @Column({ name: PRODUCT_TABLE_SCHEMA.FIELDS.NAME, nullable: false })
    public name: string;

    @Column({ name: PRODUCT_TABLE_SCHEMA.FIELDS.PRICE, nullable: false, type: "float" })
    public price: number;

    @Column({ name: PRODUCT_TABLE_SCHEMA.FIELDS.COLOR, nullable: false })
    public color: string;

    @Column({ name: PRODUCT_TABLE_SCHEMA.FIELDS.IMAGE_URL, nullable: false })
    public imageUrl: string;

    @Column({ name: PRODUCT_TABLE_SCHEMA.FIELDS.BRANCH, nullable: false })
    public branch: string;

    @Column({ name: PRODUCT_TABLE_SCHEMA.FIELDS.CATEGORY_ID, nullable: false })
    public categoryId: string;

    @Column({ name: PRODUCT_TABLE_SCHEMA.FIELDS.DESCRIPTION, nullable: false })
    public description: string;


    @Column({ name: PRODUCT_TABLE_SCHEMA.FIELDS.IS_DELETED, nullable: false })
    public isDeleted: boolean;

    @Column({ name: PRODUCT_TABLE_SCHEMA.FIELDS.IS_ENABLE, nullable: false })
    public isEnable: boolean;
}
