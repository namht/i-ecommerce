export class Converter {
    public static toModel<T1, T2>(Type: { new(p: T1): T2 }, item: T1): T2 {
        return new Type(item);
    }
}
