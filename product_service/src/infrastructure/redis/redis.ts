import { ClientOpts, Multi, RedisClient } from "redis";
import * as _ from "lodash";
const RedisLib = require("redis");

import { IOCServiceName } from '../../ioc/IocServiceName';
import Container, { Service } from 'typedi';
import { ILogger } from './../../libs/logger';
import { IConfig } from './../../configs';
const { promisify } = require('util');

// Load log and config
const systemConfig = Container.get<IConfig>(IOCServiceName.SYSTEM_CONFIG);
const Logger = Container.get<ILogger>(IOCServiceName.LIB_LOGGER);

// export interface Multi extends NodeJS.EventEmitter {
//     constructor();
//     execAsync(...args: any[]): Promise<any>;
// }
export interface IRedisClient extends NodeJS.EventEmitter {
    init();
    getClient();
    decrAsync(...args: any[]): Promise<any>;
    delAsync(...args: any[]): Promise<any>;
    execAsync(...args: any[]): Promise<any>;
    getAsync(...args: any[]): Promise<any>;
    incrAsync(...args: any[]): Promise<any>;
    expireAsync(...args: any[]): Promise<any>;
    keysAsync(...args: any[]): Promise<any>;
    saddAsync(...args: any[]): Promise<any>;
    scardAsync(...args: any[]): Promise<any>;
    sdiffAsync(...args: any[]): Promise<any>;
    sdiffstoreAsync(...args: any[]): Promise<any>;
    selectAsync(...args: any[]): Promise<any>;
    setAsync(...args: any[]): Promise<any>;
    sinterAsync(...args: any[]): Promise<any>;
    sismemberAsync(...args: any[]): Promise<any>;
    smembersAsync(...args: any[]): Promise<any>;
    smoveAsync(...args: any[]): Promise<any>;
    spopAsync(...args: any[]): Promise<any>;
    srandmemeberAsync(...args: any[]): Promise<any>;
    sremAsync(...args: any[]): Promise<any>;
    sscanAsync(...args: any[]): Promise<any>;
    sunionAsync(...args: any[]): Promise<any>;
    sunionstoreAsync(...args: any[]): Promise<any>;
    zaddAsync(...args: any[]): Promise<any>;
    zrevrangebyscoreAsync(...args: any[]): Promise<any>;
    pfaddAsync(...args: any[]): Promise<any>;
    pfcountAsync(...args: any[]): Promise<any>;
    pfmergeAsync(...args: any[]): Promise<any>;
}

@Service(IOCServiceName.REDIS_RESPOSITORY)
export class RedisWrapper {
    private writeConnection: RedisConnection;
    private readConnection: RedisConnection;

    public async init(): Promise<any> {
        const readConnection =  new RedisConnection(systemConfig.getRedisReadConfig);
        const writeConnection = new RedisConnection(systemConfig.getRedisWriteConfig);
        await Promise.all([
            readConnection.initWithWaiting("Read"),
            writeConnection.initWithWaiting("Write")
        ]);
        this.readConnection = readConnection;
        this.writeConnection = writeConnection;
    }

    public getClient() {
        return this;
    }

    public multi(...args: any[]): Multi {
        return this.writeConnection.getClient().promise.multi(args);
    }

    public decrAsync(...args: any[]): Promise<any> {
        return this.writeConnection.getClient().promise.decr(args);
    }

    public delAsync(...args: any[]): Promise<any> {
        return this.writeConnection.getClient().promise.del(args);
    }

    public execAsync(...args: any[]): Promise<any> {
        return this.writeConnection.getClient().promise.exec(args);
    }

    public getAsync(...args: any[]): Promise<any> {
        return this.readConnection.getClient().promise.get(args);
    }

    public incrAsync(...args: any[]): Promise<any> {
        return this.writeConnection.getClient().promise.incr(args);
    }

    public expireAsync(...args: any[]): Promise<any> {
        return this.writeConnection.getClient().promise.expire(args);
    }

    public keysAsync(...args: any[]): Promise<any> {
        return this.readConnection.getClient().promise.keys(args);
    }

    public saddAsync(...args: any[]): Promise<any> {
        return this.writeConnection.getClient().promise.sadd(args);
    }

    public scardAsync(...args: any[]): Promise<any> {
        return this.readConnection.getClient().promise.scard(args);
    }

    public sdiffAsync(...args: any[]): Promise<any> {
        return this.readConnection.getClient().promise.sdiff(args);
    }

    public sdiffstoreAsync(...args: any[]): Promise<any> {
        return this.writeConnection.getClient().promise.sdiffstore(args);
    }

    public selectAsync(...args: any[]): Promise<any> {
        return this.writeConnection.getClient().promise.select(args);
    }

    public setAsync(...args: any[]): Promise<any> {
        return this.writeConnection.getClient().promise.set(args);
    }

    public sinterAsync(...args: any[]): Promise<any> {
        return this.readConnection.getClient().promise.sinter(args);
    }

    public sismemberAsync(...args: any[]): Promise<any> {
        return this.readConnection.getClient().promise.sismember(args);
    }

    public smembersAsync(...args: any[]): Promise<any> {
        return this.readConnection.getClient().promise.smembers(args);
    }

    public smoveAsync(...args: any[]): Promise<any> {
        return this.writeConnection.getClient().promise.smove(args);
    }

    public spopAsync(...args: any[]): Promise<any> {
        return this.writeConnection.getClient().promise.spop(args);
    }

    public srandmemeberAsync(...args: any[]): Promise<any> {
        return this.readConnection.getClient().promise.srandmemeber(args);
    }

    public sremAsync(...args: any[]): Promise<any> {
        return this.writeConnection.getClient().promise.srem(args);
    }

    public sscanAsync(...args: any[]): Promise<any> {
        return this.writeConnection.getClient().promise.sscan(args);
    }

    public sunionAsync(...args: any[]): Promise<any> {
        return this.readConnection.getClient().promise.sunion(args);
    }

    public sunionstoreAsync(...args: any[]): Promise<any> {
        return this.writeConnection.getClient().promise.sunionstore(args);
    }

    public zaddAsync(...args: any[]): Promise<any> {
        return this.writeConnection.getClient().promise.zadd(args);
    }

    public zrevrangebyscoreAsync(...args: any[]): Promise<any> {
        return this.readConnection.getClient().promise.zrevrangebyscore(args);
    }

    public pfaddAsync(...args: any[]): Promise<any> {
        return this.writeConnection.getClient().promise.pfadd(args);
    }

    public pfcountAsync(...args: any[]): Promise<any> {
        return this.readConnection.getClient().promise.pfcount(args);
    }

    public pfmergeAsync(...args: any[]): Promise<any> {
        return this.writeConnection.getClient().promise.pfmerge(args);
    }

    /**
     * Create a unique key in redis
     * @param params array of string
     */
    private createKey(...params: string[]): string {
        return params.filter(val => val != null && val !== "").join(":");
    }

    /**
     *
     * @param params
     * @returns {string}
     */
    private createCacheKey(...params: string[]): string {
        return this.createKey("cache", ...params);
    }

    /**
     *
     * @param userId
     * @returns {string}
     */
    public sessionSetKey(userId: string): string {
        return this.createKey("sessions", userId);
    }

    /**
     *
     * @param whatOnId
     * @returns {string}
     */
    public getWhatOnCountKey(whatOnId: string) {
        return this.createKey("whatOn", "counter", whatOnId);
    }

    /**
     *
     * @param announcementId
     * @returns {string}
     */
    public getAnnouncementCountKey(announcementId: string) {
        return this.createKey("announcement", "counter", announcementId);
    }

    /**
     *
     * @param garageSaleId
     * @returns {string}
     */
    public getGarageSaleLikeKey(garageSaleId: string) {
        return this.createKey("garageSale", "like", garageSaleId);
    }

    /**
     *
     * @param type
     * @param feedId
     * @returns {string}
     */
    public getFeedPostKey(type: string, feedId: string) {
        return this.createKey("feed", type.toString().toLowerCase(), "post", feedId);
    }

    /**
     *
     * @param type
     * @param feedId
     * @returns {string}
     */
    public getFeedLikeKey(type: string, feedId: string) {
        return this.createKey("feed", type.toString().toLowerCase(), "like", feedId);
    }

    /**
     *
     * @param type
     * @param feedId
     * @returns {string}
     */
    public getFeedLikesKey(type: string, feedId: string) {
        return this.createKey("feed", type.toString().toLowerCase(), "likes", feedId);
    }

    /**
     * The key save the user like the feed.
     *
     * @param type
     * @param feedId
     * @returns {string}
     */
    public getFeedLikeByUserKey(type: string, feedId: string, userLike: string) {
        return this.createKey("feed", type.toString().toLowerCase(), "like", feedId, "by", userLike);
    }

    /**
     *
     * @param type
     * @param feedId
     * @returns {string}
     */
    public getFeedCommentKey(type: string, feedId: string) {
        return this.createKey("feed", type.toString().toLowerCase(), "comment", feedId);
    }

    /**
     *
     * @param feedId
     * @returns {string}
     */
    public getUserCanChatOnFeed(feedId: string): string {
        return this.createKey("feed", feedId, "canChatUsers");
    }

    /**
     *
     * @param type
     * @param feedId
     * @returns {string}
     */
    public getFeedCommentCounterKey(type: string, feedId: string) {
        return this.createKey("feed", type.toString().toLowerCase(), "comment", "counter", feedId);
    }

    /**
     *
     * @param type
     * @param feedId
     * @returns {string}
     */
    public getFeedCommentLikeKey(type: string, feedId: string, commentId: string) {
        return this.createKey("feed", type.toString().toLowerCase(), "comment", feedId, "like", commentId);
    }

    /**
     *
     * @param type
     * @param feedId
     * @returns {string}
     */
    public getFeedCommentLikesKey(type: string, feedId: string, commentId: string) {
        return this.createKey("feed", type.toString().toLowerCase(), "comment", feedId, "likes", commentId);
    }

    /**
     *
     * @param level
     * @param id
     * @param slotId
     * @param week
     * @returns {string}
     */
    public getBookingRateLimiterKey(level: string, owner: string, kind: string, targetlLevel: string, target: string, bookingRestrictUnitId: string, value: string): string {
        return this.createKey("booking", "limiter", level, owner, kind, targetlLevel, target, bookingRestrictUnitId, value);
    }

    /**
     *
     * @param userId
     * @param slotId
     * @returns {string}
     */
    public getCacheBookingRatemiterSetKey(userId: string, facilityId: string, stamp: string): string {
        return this.createCacheKey("booking", "limiter", userId, facilityId, stamp.toString());
    }

    public getUserTokenKey(userId: string, token: string): string {
        return this.createKey("users", userId, "tokens", token);
    }

    public getUserRoleSetKey(userId: string): string {
        return this.createKey("users", userId, "roles");
    }

    public getUserCondoKey(userId: string): string {
        return this.createKey("users", userId, "condoId");
    }

    public getClusteringSetKey(clusterId: string): string {
        return this.createKey("cluster", clusterId);
    }

    public getCondoClusterSetKey(condoId: string, type: string): string {
        return this.createKey("condo", condoId, "cluster", type);
    }

    public getPinKey(phoneNumber: string): string {
        return this.createKey("phoneNumber", phoneNumber);
    }

    public getAdminTokenKey(token: string): string {
        return this.createKey("adminToken", token);
    }

    public getLatestTransactionKey(condoId: string): string {
        return this.createKey("latestTransaction", condoId);
    }

    public getCounterDepositKey(condoId: string): string {
        return this.createKey("counterDeposit", condoId);
    }

    public getFeedbackReadersKey(feedbackId: string): string {
        return this.createKey("feedbackReaders", feedbackId);
    }

    public getNewsLikeKey(newsId: string) {
        return this.createKey("news", "like-news", newsId);
    }

    public getNewsLikeByUserKey(userId: string) {
        return this.createKey("news", "like-users", userId);
    }

    // TODO Will updated/removed this method after multi condo is applied completely.
    public getActiveUserUnitKey(userId: string) {
        return this.createKey("users", userId, "unit", "active");
    }

    public getTrackingOnlineDetailKey(userType: string) {
        return this.createKey("tracking", "online", "detail", userType);
    }

    public getTrackingOnlineCountKey(time: string, userType: string) {
        return this.createKey("tracking", "online", "count", userType, time);
    }

    public getTrackingLastLoginKey(userId: string) {
        return this.createKey("tracking", "online", "lastLogin", userId);
    }

    public getUserTotalNotificationCount(userId: string, groupType: string) {
        return this.createKey("notifications", "total", userId, groupType);
    }

    public getNotificationIdKey(id: string) {
        return this.createKey("notifications", "ids", id);
    }

    public getUserLastReadAll(userId: string, condoId: string, groupType: string) {
        return this.createKey("notifications", "read", userId, condoId, groupType);
    }
}
interface RedisOpts extends ClientOpts {
    prefix: string;
    host: string;
    port: number;
    db: string;
}


export class RedisConnection {
    private opts: RedisOpts;
    private client;
    public prefix: string;
    public db: string;

    constructor(opts?: any) {
        opts = opts || {};
        let defaultOpts: RedisOpts = {
            host: "localhost",
            port: 6379,
            db: "1",
            prefix: process.env.NODE_ENV + ":icondo:service:v1:",
        };

        this.opts = _.defaultsDeep(opts, defaultOpts) as RedisOpts;
        if (process.env.REDIS_HOST != null) {
            this.opts.host = process.env.REDIS_HOST;
        }
        if (process.env.REDIS_PORT != null) {
            this.opts.port = Number.parseInt(process.env.REDIS_PORT, 10);
        }
        if (process.env.REDIS_DB != null) {
            this.opts.db = process.env.REDIS_DB;
        }
        this.prefix = this.opts.prefix;
        this.db = this.opts.db;
    }

    public initWithWaiting(typeName: string): Promise<boolean> {
        Logger.warn(`Wait for redis ${typeName} connection`);
        this.client = RedisLib.createClient({
            host: this.opts.host,
            port: this.opts.port,
            db: this.opts.db,
            prefix: this.opts.prefix,
            retry_strategy: function (options) {
                if (options.error && options.error.code === 'ECONNREFUSED') {
                    // End reconnecting on a specific error and flush all commands with a individual error
                    return new Error('The server refused the connection');
                }
                if (options.total_retry_time > 1000 * 60 * 60) {
                    // End reconnecting after a specific timeout and flush all commands with a individual error
                    return new Error('Retry time exhausted');
                }
                // attempt reconnect after retry_delay, and flush any pending commands
                return {
                    retry_delay: Math.min(options.attempt * 100, 3000),
                    error: new Error('Your custom error.')
                }
            }
        });
        this.client.promise = Object.entries(RedisLib.RedisClient.prototype)
            .filter(([_, value]) => typeof value === 'function')
            .reduce((acc, [key, value]) => ({
                ...acc,
                [key]: promisify(value).bind(this.client)
            }), {});
        // this.client.on('connect', (msg) => {
        //     Logger.info("Redis connection is OK");
        // });
        this.client.on('ready', (msg) => {
            Logger.info(`Redis ${typeName} connection is OK`);
        });
        this.client.on('reconnecting', (msg) => {
            Logger.info(`Redis ${typeName} reconnecting ...`);
        });
        this.client.on('error', (msg) => {
            Logger.info(`Connect to redis ${typeName} failed, try again`);
            Logger.error(msg);
        });
        return Promise.resolve(true);
        // this.client.on('end', redisCallbackHandler);
            
    }
    
    public getClient() {
        return this.client;
    }
}