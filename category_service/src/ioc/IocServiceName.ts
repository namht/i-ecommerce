export const IOCServiceName = {
    PRODUCT_SERVICE: 'product.service',
    CATEGORY_SERVICE: 'category.service',

    // system
    SYSTEM_CONFIG: 'system.config',
    // postgres
    DATABASE_CONNECTION: 'database.connection',

    // repository
    PRODUCT_REPOSITORY: 'product.repository',
    CATEGORY_REPOSITORY: 'category.repository',

    // Redis
    REDIS_RESPOSITORY: "redis.respository",

    // lib
    LIB_LOGGER: 'lib.logger',
};
