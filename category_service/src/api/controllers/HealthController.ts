import { Get, JsonController } from 'routing-controllers';
@JsonController('/health')
export class HealthController {
    @Get('/', {transformResponse: true})
    public async ping():Promise<any> {
        return {};
    }
}
