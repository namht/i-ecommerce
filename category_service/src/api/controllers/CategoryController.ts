import { Request, Response, urlencoded } from 'express';
import { Get, Post, Put, Delete, JsonController, Req, Res, Body } from 'routing-controllers';
import { Inject, Service } from 'typedi';
import { ICategoryService } from '../../app/interfaces';
import { Exception, CategoryModel } from '../../app/models';
import { IOCServiceName } from '../../ioc/IocServiceName';
import { ErrorCode, HTTP_CODE, TIME_ZONE } from '../../app/constants';

@Service()
@JsonController('/categories')
export class CategoryController {
    @Inject(IOCServiceName.CATEGORY_SERVICE)
    private CategoryService: ICategoryService;

    @Get('/', { transformResponse: true })
    public async list(@Req() req: Request, @Res() response: Response): Promise<any> {
        let offset = parseInt(req.query.offset as string, 10) || null!;
        let limit = parseInt(req.query.limit as string, 10) || null!;
        let queryParams = req.query || null;

        return this.CategoryService.search({}, queryParams, offset, limit, []);
    }

    @Post('/', { transformResponse: true })
    public async create(@Req() req: Request, @Res() response: Response, @Body() reqBody: any): Promise<any> {
        if (!reqBody.name) {
            throw new Exception(
                ErrorCode.RESOURCE.MISSING_REQUIRED_FIELDS.CODE,
                ErrorCode.RESOURCE.MISSING_REQUIRED_FIELDS.MESSAGE,
                false,
                HTTP_CODE.BAD_REQUEST
            );
        }

        let createData = new CategoryModel(reqBody);
        createData.isDeleted = false;
        createData.isEnable = true;
        return this.CategoryService.create({}, createData);
    }

    @Get('/:id', { transformResponse: true })
    public async get(@Req() req: Request, @Res() response: Response): Promise<any> {
        let { id } = req.params;
        if (!id) {
            throw new Exception(
                ErrorCode.RESOURCE.MISSING_REQUIRED_FIELDS.CODE,
                ErrorCode.RESOURCE.MISSING_REQUIRED_FIELDS.MESSAGE,
                false,
                HTTP_CODE.BAD_REQUEST
            );
        }

        return this.CategoryService.detail({}, id);
    }

    @Put('/:id', { transformResponse: true })
    public async update(@Req() req: Request, @Res() response: Response,  @Body() reqBody: any): Promise<any> {
        let { id } = req.params;
        if (!id) {
            throw new Exception(
                ErrorCode.RESOURCE.MISSING_REQUIRED_FIELDS.CODE,
                ErrorCode.RESOURCE.MISSING_REQUIRED_FIELDS.MESSAGE,
                false,
                HTTP_CODE.BAD_REQUEST
            );
        }

        let category = await this.CategoryService.detail({}, id);
        if (!category) {
            throw new Exception(
                ErrorCode.RESOURCE.NOT_FOUND.CODE,
                ErrorCode.RESOURCE.NOT_FOUND.MESSAGE,
                false,
                HTTP_CODE.BAD_REQUEST
            );
        }
        if (reqBody.name) {
            category.name = reqBody.name;
        }
        
        return this.CategoryService.update({}, category);
    }

    @Delete('/:id', { transformResponse: true })
    public async delete(@Req() req: Request, @Res() response: Response): Promise<any> {
        let { id } = req.params;
        if (!id) {
            throw new Exception(
                ErrorCode.RESOURCE.MISSING_REQUIRED_FIELDS.CODE,
                ErrorCode.RESOURCE.MISSING_REQUIRED_FIELDS.MESSAGE,
                false,
                HTTP_CODE.BAD_REQUEST
            );
        }

        let category = await this.CategoryService.detail({}, id);
        if (!category) {
            throw new Exception(
                ErrorCode.RESOURCE.NOT_FOUND.CODE,
                ErrorCode.RESOURCE.NOT_FOUND.MESSAGE,
                false,
                HTTP_CODE.BAD_REQUEST
            );
        }
        category.isDeleted = true;

        return this.CategoryService.update({}, category);
    }
}
