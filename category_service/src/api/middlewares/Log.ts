import * as express from 'express';
import * as _ from 'lodash';
import * as UUID from 'uuid';
import { ErrorCode, HTTP_CODE } from '../../app/constants';
import { Exception } from '../../app/models/common/Exception';

interface LogHandler {
    error(message: string, meta?: any): void;
    warn(message: string, meta?: any): void;
    info(message: string, meta?: any): void;
}

export const httpLogger = (logger?: LogHandler): express.RequestHandler => {
    const Logger: LogHandler = logger || {
        error: (message: string, meta?: any): void => console.error(message),
        warn: (message: string, meta?: any): void => console.warn(message),
        info: (message: string, meta?: any): void => console.log(message),
    };

    return (req: express.Request, res: any, next: express.NextFunction) => {
        res.locals.id = UUID.v4();
        res.locals.method = req.method;
        res.locals.ip = req.ip;
        res.locals.hostname = req.hostname;
        res.locals.protocol = req.protocol;
        req['id'] = res.locals.id;
        req['_startTime'] = Date.now();

        // Capture end function in Request object to calculate information
        let endFunc = res.end;
        res.end = (chunk: any, encoding: any) => {
            res.responseTime = Date.now() - req['_startTime'];
            res.end = endFunc;
            res.end(chunk, encoding);
            req.url = req.originalUrl || req.url;

            const format = `${req['id']} ${req.ip} ${req.method} ${req.url} ${res.responseTime}ms ${chunk ? chunk.length : 0}bytes ${res.statusCode} ${res.statusMessage}`;
            const meta = {
                ip: req.ip,
                method: req.method,
                path: req.path ? req.path : '',
                time: res.responseTime,
                size: chunk ? chunk.length : 0,
                statusCode: res.statusCode,
                headers: req.headers,
                body: req.body,
            };
            switch (true) {
            case (req.path === '/api/v1/health'):
                break;
            case (res.statusCode < 200):
                Logger.warn(format, meta);
                break;
            case (res.statusCode > 199 && res.statusCode < 300):
                Logger.info(format, meta);
                break;
            case (res.statusCode > 299 && res.statusCode < 500):
                Logger.warn(format, meta);
                break;
            default:
                Logger.error(format, meta);
            }

            endFunc = null;
        };
        next();
    };
};

export const httpError = (logger?: any): express.ErrorRequestHandler => {
    const Logger = logger || {
        error: (message: string, meta?: any): void => console.error(message),
        warn: (message: string, meta?: any): void => console.warn(message),
        info: (message: string, meta?: any): void => console.log(message),
    };

    return (error: any, req: express.Request, res: express.Response, next: express.NextFunction): any => {
        if (!(_.map(ErrorCode, (o) => o.GENERIC.CODE).find((code) => code === error.code))) {
            Logger.warn(error.message, error);
        }
        else {
            Logger.error(error.message, error);
        }
        if (error.httpStatus == null) {
            error = Exception.fromError(
                ErrorCode.RESOURCE.GENERIC.CODE,
                error,
                true,
                HTTP_CODE.INTERNAL_SERVER_ERROR
            );

            if (process.env.NODE_ENV === 'production') {
                error.message = ErrorCode.RESOURCE.GENERIC.MESSAGE;
            }
        }
        next(error);
    };
};
