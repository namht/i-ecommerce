import * as express from 'express';
import { ExpressMiddlewareInterface, Middleware } from 'routing-controllers';
import { ErrorCode } from '../../app/constants';
import { Exception } from '../../app/models/common/Exception';

export const notFound = (): express.RequestHandler => {
    const notFoundMiddleware = (req: express.Request, res: express.Response, next: express.NextFunction): any => {
        if (!res.headersSent) {
            next(new Exception(
                ErrorCode.RESOURCE.INVALID_URL.CODE,
                ErrorCode.RESOURCE.INVALID_URL.MESSAGE,
                false,
                404,
            ));
        }
    };
    return notFoundMiddleware;
};
@Middleware({type: 'after'})
export class notFoundMiddleware implements ExpressMiddlewareInterface {
    public use(request: any, response: any, next: (err?: any) => any): any {
        if (!response.headersSent) {
            next(new Exception(
                ErrorCode.RESOURCE.INVALID_URL.CODE,
                ErrorCode.RESOURCE.INVALID_URL.MESSAGE,
                false,
                404,
            ));
        }
    }
}

export default notFound;
