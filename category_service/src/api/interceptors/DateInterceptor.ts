import {InterceptorInterface, Action, Interceptor} from 'routing-controllers';
import * as momentTz from 'moment-timezone';
import * as _ from 'lodash';
import {BaseInterceptor} from './BaseInterceptor';
@Interceptor()
export class DateCorrectionInterceptor extends BaseInterceptor implements InterceptorInterface {
    intercept(action: Action, data: any) {
        this.nestedLoopAndHandler(data, (currentData, key, value): boolean => {
            if (momentTz.isMoment(value)) {
                currentData[key] = value.toISOString();
            }
            if (_.isObject(value)) {
                return true;
            }
            return false;
        });
        return data;
    }
}
