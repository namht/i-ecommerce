import * as _ from "lodash";
export abstract class BaseInterceptor {
    protected nestedLoopAndHandler(data, handler: (currentData: any, key: string, value: any) => boolean) {
        _.forOwn(data, (value, key) => {
            if (handler(data, key, value)) {
                this.nestedLoopAndHandler(data[key], handler);
            }
        });
    }
}
