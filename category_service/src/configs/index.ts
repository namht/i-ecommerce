import Container from 'typedi';
import * as yaml from 'js-yaml';
import * as path from 'path';
import * as fs from 'fs';
import {IOCServiceName} from '../ioc/IocServiceName';
import Logger  from "../libs/Logger";

export interface IPostgresConfig {
    host: string;
    port: number;
    user: string;
    password: string;
    database: string;
    charset: string;
    debug: boolean;
    redisCache: IRedisConfig;
}
export interface IRedisConfig {
    host: string;
    port: number;
    db: string;
    prefix: string;
}


export interface IConfig {
    port: number;
    getPostgresConfig(): IPostgresConfig;
    getRedisReadConfig(): IRedisConfig;
    getRedisWriteConfig(): IRedisConfig;
    init(): void;
}

export class Config implements IConfig {
    public port: number;
    private _postgresConfig: IPostgresConfig;
    private _redisReadConfig: IRedisConfig;
    private _redisWriteConfig: IRedisConfig;

    constructor() {
        console.log('get contructor configuration .yaml ===>');
    }
    public init(): void {
        Logger.info('init configuration .yaml ======>');
        const url = path.join(__dirname, './env');
        const conf: any = {};
        if (process.env.NODE_ENV == null) {
            process.env.NODE_ENV = 'development';
        }
        try {
            const doc = yaml.safeLoad(fs.readFileSync(`${url}/${process.env.NODE_ENV}.yaml`, 'utf8'));
            if (typeof doc == "object")
            for (const key of Object.keys(doc)) {
                const val = doc[key];
                if (val != null) {
                    conf[key] = val;
                }
            }
        }
        catch (err) {
            Logger.error(`Error when loading configuration file ${process.env.NODE_ENV}.yaml, fallback to configuration.yaml`);
            try {
                const doc = yaml.safeLoad(fs.readFileSync(`${url}/configuration.yaml`, 'utf8'));
                if (typeof doc == "object")
                for (const key of Object.keys(doc)) {
                    const val = doc[key];
                    if (val != null) {
                        conf[key] = val;
                    }
                }
            }
            catch (err) {
                Logger.error(`Error when loading configuration file configuration.yaml, using default value for each module: ${err.message}`);
            }
        }

        this.setPostgresConfig(conf?.database?.postgres);
        this.setRedisReadConfig(conf?.database?.redisRead);
        this.setRedisWriteConfig(conf?.database?.redis);
        this.port = conf?.PORT ? Number(conf?.PORT) : 3000;
    }

    public setPostgresConfig(config: IPostgresConfig): void {
        this._postgresConfig = config;
    }

    public setRedisReadConfig(config: IRedisConfig): void {
        this._redisReadConfig = config;
    }
    public setRedisWriteConfig(config: IRedisConfig): void {
        this._redisWriteConfig = config;
    }

    public getPostgresConfig(): IPostgresConfig {
        if (!this._postgresConfig) {
            throw new Error('missing init Postgres config ===>');
        }
        return this._postgresConfig;
    }

    public getRedisReadConfig(): IRedisConfig {
        if (!this._redisReadConfig) {
            throw new Error('missing init redis read config ===>');
        }
        return this._redisReadConfig;
    }

    public getRedisWriteConfig(): IRedisConfig {
        if (!this._redisWriteConfig) {
            throw new Error('missing init redis read config ===>');
        }
        return this._redisWriteConfig;
    }
}

const config = new Config();
config.init();
Container.set(IOCServiceName.SYSTEM_CONFIG, config);
