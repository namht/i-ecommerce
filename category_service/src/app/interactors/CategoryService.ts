import Container, { Inject, Service } from 'typedi';
import { IOCServiceName } from '../../ioc/IocServiceName';
import { ICategoryService} from '../interfaces';
import { ICategoryRepository } from '../interfaces/repository';
import { IContext } from "../interfaces/common/IContext";
import * as _ from "lodash";
import { Exception as ExceptionModel, CategoryModel, CollectionWrap } from '../models';

@Service(IOCServiceName.CATEGORY_SERVICE)
class CategoryService implements ICategoryService {
    @Inject(IOCServiceName.CATEGORY_REPOSITORY)
    private readonly CategoryRepository: ICategoryRepository;
    public async search(ctx: IContext, searchParams: any, offset: number, limit: number, related): Promise<CollectionWrap<CategoryModel>> {
        return this.CategoryRepository.search(ctx, searchParams, offset, limit, related);
    };

    public async detail(ctx: IContext, id: string): Promise<CategoryModel | undefined> {
        return this.CategoryRepository.get(id);
    };

    public async update(ctx: IContext, data: CategoryModel): Promise<CategoryModel> {
        return this.CategoryRepository.save(data);
    };

    public async create(ctx: IContext, data: CategoryModel): Promise<CategoryModel> {
        return  this.CategoryRepository.create(data);
    };

    public async delete(ctx: IContext, id: string): Promise<CategoryModel> {
        return this.CategoryRepository.delete(id);
    };
}
export default CategoryService;