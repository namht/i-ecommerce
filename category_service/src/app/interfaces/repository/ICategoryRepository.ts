import { CategoryModel, CollectionWrap } from '../../models';
import { IBaseRepository } from './IBaseRepository';
import { IContext } from "../common/IContext";

export interface ICategoryRepository extends IBaseRepository<CategoryModel> {
    search(ctx: IContext, searchParams: any, offset: number, limit: number, relate: string[]): Promise<CollectionWrap<CategoryModel>>;
}
