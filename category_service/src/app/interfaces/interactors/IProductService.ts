import { ProductModel, CollectionWrap } from "../../models";
import { IContext } from "../common/IContext";
export interface IProductService {
    search(ctx: IContext, searchParams: any, offset: number, limit: number, related: string[]): Promise<CollectionWrap<ProductModel>>;
}