import { CategoryModel, CollectionWrap } from "../../models";
import { IContext } from "../common/IContext";
export interface ICategoryService {
    search(ctx: IContext, searchParams: any, offset: number, limit: number, related: string[]): Promise<CollectionWrap<CategoryModel>>;
    detail(ctx: IContext, id: string): Promise<CategoryModel | undefined>;
    update(ctx: IContext, data: CategoryModel): Promise<CategoryModel>;
    create(ctx: IContext, data: CategoryModel): Promise<CategoryModel>;
    delete(ctx: IContext, id: string): Promise<CategoryModel>;
}