import { CategoryModel } from "../../models"
import * as momentTz from "moment-timezone";
export interface ICategoryModel {
    id: string;
    createdDate: momentTz.Moment;
    updatedDate: momentTz.Moment;
    isDeleted: boolean;
    isEnable: boolean;
    name: string;
}