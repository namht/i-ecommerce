// import { IUserModel } from "./IUserModel";
// import { IUserManagerModel } from "./IUserManagerModel";
// import { IGetQuotationServiceModel } from "./IGetQuotationServiceModel";
import * as momentTz from "moment-timezone";
export interface IProductModel {
    id: string;
    createdDate: momentTz.Moment;
    updatedDate: momentTz.Moment;
    isDeleted: boolean;
    isEnable: boolean;
    name: string;
    price: number;
    color: string;
    branch: string;
    categoryId: string;
    imageUrl: string;
    // category: ICategoryModel;
}