import { Validator } from 'class-validator';
import { Container } from 'typedi';
// import { UserModel } from './UserModel';
import { BaseModel } from './BaseModel';
import * as momentTz from "moment-timezone";
import { ICategoryModel } from '../interfaces/models';
const validator = Container.get(Validator);

export class CategoryModel extends BaseModel implements ICategoryModel {
    constructor(data?: any) {
        super();
        if(data) {
            const keys = Object.keys(data);
            keys.forEach(key => {
                this[key] = data[key];
            });
        }
    }
    public name: string;
}

export default CategoryModel;