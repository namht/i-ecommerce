import { Validator } from 'class-validator';
import { Container } from 'typedi';
// import { UserModel } from './UserModel';
import { BaseModel } from './BaseModel';
import * as momentTz from "moment-timezone";
import { IProductModel } from '../interfaces/models';
const validator = Container.get(Validator);

export class ProductModel extends BaseModel implements IProductModel {
    constructor(data?: any) {
        super();
        if(data) {
            const keys = Object.keys(data);
            keys.forEach(key => {
                this[key] = data[key];
            });
        }
    }
    public name: string;
    public price: number;
    public color: string;
    public branch: string;
    public description: string;
    public categoryId: string;
    public imageUrl: string;

    // public category: UserModel;
}

export default ProductModel;