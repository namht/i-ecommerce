class Code {
    public readonly CODE;
    public readonly MESSAGE;

    constructor(code, message) {
        this.CODE = code;
        this.MESSAGE = message;
    }
}

export const ErrorCode = {
    UNKNOWN: {
        TYPE: 'Unknown.',
        GENERIC: new Code(0, 'Internal Server Error.'),
        NOT_IMPLEMENT: new Code(1, 'Not Implement ExceptionModel'),
        UPGRADE_NEEDED: new Code(2, 'Please update new version'),
        MAINTENANCE_PERIOD: new Code(3, 'Maintenance period'),
        APOCALYPSE: new Code(13, 'The world is end'),
    },
    RESOURCE: {
        TYPE: 'Resource.',
        GENERIC: new Code(1000, 'unknown Resource\'s Error.'),
        INVALID_URL: new Code(1001, 'invalid url.'),
        NOT_FOUND: new Code(1002, 'the item has been deleted'),
        DUPLICATE_RESOURCE: new Code(1003, 'resource was already existed.'),
        INVALID_REQUEST: new Code(1004, 'invalid access.'),
        MISSING_REQUIRED_FIELDS: new Code(1005, 'missing required field.'),
    },
    AUTHENTICATION: {
        TYPE: 'Authentication.',
        GENERIC: new Code(1100, 'unknown authentication\'s error.'),
        VIOLATE_RFC6750: new Code(1101, 'RFC6750 states the access_token MUST NOT be provided in more than one place in a single request.'),
        TOKEN_NOT_FOUND: new Code(1102, 'token not found.'),
        INVALID_AUTHORIZATION_HEADER: new Code(1103, 'invalid authorization header.'),
    },
    PRIVILEGE: {
        TYPE: 'Privilege',
        GENERIC: new Code(1200, 'unknown privilege\'s error.'),
        NOT_ALLOW: new Code(1201, 'you do not have permission to access.'),
        NOT_ALLOW_CONDO_LESS: new Code(1202, 'you do not have a condo to access.'),
        INVALID_VERSION: new Code(1204, 'invalid version format'),
        VERSION_NOT_FOUND: new Code(1205, 'version not found'),
        FORCE_UPDATE: new Code(1206, 'please update to the latest version')
    }
};
