import Container from 'typedi';
import { ApiRouting } from './api';
import { IDatabaseConnection } from './infrastructure/postgres/typeorm/connection';
import { IOC } from './ioc';
import { IOCServiceName } from './ioc/IocServiceName';
import { ILogger } from './libs/logger';
import { IConfig } from './configs';
// import { IRedisClient } from './infrastructure/redis/redis';

async function startApplication() {

    // 1. Init Dependency Injection 
    new IOC().setup();
    // Load log and config
    const systemConfig = Container.get<IConfig>(IOCServiceName.SYSTEM_CONFIG);
    const logger = Container.get<ILogger>(IOCServiceName.LIB_LOGGER);

     // 2. Init Connection Database
     const databaseConnection = Container.get<IDatabaseConnection>(IOCServiceName.DATABASE_CONNECTION);
     await databaseConnection.createConnection();

    // 3. Init Connection Redis
    // const redisConnection = Container.get<IRedisClient>(IOCServiceName.REDIS_RESPOSITORY);
    // await redisConnection.init();

    // 4. Setup Api
    new ApiRouting(logger).setup(systemConfig.port);

}
startApplication();