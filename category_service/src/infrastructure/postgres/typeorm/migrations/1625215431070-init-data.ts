import {MigrationInterface, QueryRunner} from "typeorm";

export class initData1625215431037 implements MigrationInterface {
    name = 'initData1625215431037'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
        INSERT INTO "public"."categories" VALUES ('64584ae5-a58a-480d-a23f-c172e15a6d92', '2021-07-02 09:04:59.5337+00', '2021-07-02 09:04:59.5337+00', 'Food', 'f', 't');
        INSERT INTO "public"."categories" VALUES ('0c40d69c-aac6-42e9-9a15-643bb8721c7f', '2021-07-02 09:05:13.84802+00', '2021-07-02 09:05:13.84802+00', 'Drink', 'f', 't');
        INSERT INTO "public"."categories" VALUES ('4a39a840-2072-44b6-89c4-04f8b7d67f3d', '2021-07-02 09:06:49.129015+00', '2021-07-08 16:05:02.597705+00', 'Clothes', 'f', 't');
        `);

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }
}
