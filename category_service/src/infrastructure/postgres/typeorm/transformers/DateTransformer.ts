import * as momentTz from 'moment-timezone';

export class DateTransformer {
    public from = (date: Date): momentTz.Moment | undefined => (momentTz.isMoment(momentTz(date)) ? momentTz(date) : undefined);
    public to = (date: momentTz.Moment): Date | undefined => (momentTz.isMoment(date) ? date.toDate() : undefined);
}
