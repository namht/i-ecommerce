export const CATEGORY_TABLE_SCHEMA = {
    TABLE_NAME: "categories",
    FIELDS: {
        ID: "id",
        IS_ENABLE: "is_enable",
        IS_DELETED: "is_deleted",
        CREATED_DATE: "created_date",
        UPDATED_DATE: "updated_date",
        NAME: "name"
    }
};