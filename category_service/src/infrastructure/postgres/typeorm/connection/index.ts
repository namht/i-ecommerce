import * as path from 'path';
import Container, { Service } from 'typedi';
import { Connection, createConnection, getConnection } from 'typeorm';
import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';
import { DATABASE_CONNECTION } from '../../../../app/constants';
import { IConfig } from '../../../../configs';
import { IOCServiceName } from '../../../../ioc/IocServiceName';
import Logger  from "../../../../libs/logger";
export interface IDatabaseConnection {
    getConnection(name: string): Connection;
    createConnection(): Promise<Connection>;
    clearCaching(keyCaching: string): Promise<void>;
}

export const CONNECTION_NAME = DATABASE_CONNECTION.DEFAULT;
@Service(IOCServiceName.DATABASE_CONNECTION)
export class DataBaseConnection implements IDatabaseConnection {
    getConnection(connectionName: string): Connection {
        let connection: Connection | undefined;
        try {
            connection = getConnection(connectionName);
        }
        catch { }
        if (!connection || !connection.isConnected)
            throw new Error(`Connection name ${connectionName} does not exists`);
        return connection;
    }

    async createConnection(): Promise<Connection> {
        let connection: Connection | undefined;
        try {
            connection = getConnection(CONNECTION_NAME);
        }catch(err){ };

        if (connection && connection.isConnected)
            return connection;

        const config: IConfig = Container.get(IOCServiceName.SYSTEM_CONFIG);
        const postgresConfig = config.getPostgresConfig();
        Logger.warn("Wait for postgres connection");
        return await createConnection({
            name: DATABASE_CONNECTION.DEFAULT,
            type: 'postgres',
            host: postgresConfig.host,
            port: postgresConfig.port,
            database: postgresConfig.database,
            username: postgresConfig.user,
            password: postgresConfig.password,
            cache: false,
            synchronize: false,
            logging: postgresConfig.debug,
            entities: [
                path.join(__dirname, '../entities/*{.js,.ts}')
            ],
            migrations: [
                path.join(__dirname, '../migrations/*{.js,.ts}')
            ],
            subscribers: [
                path.join(__dirname, '../subscribers/*{.js,.ts}')
            ]
        } as PostgresConnectionOptions)
        .then(connection => {
            Logger.info("Database connected!!!");
            return connection;
        });
    }

    async clearCaching(keyCaching: string): Promise<void> {
        if (!keyCaching)
            throw new Error('Cache error!');

        const connection = getConnection();
        if (!connection.queryResultCache)
            throw new Error('The database caching is disabled!');

        await connection.queryResultCache.remove([keyCaching]);
    }
}
