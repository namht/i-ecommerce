import { ICategoryModel } from 'src/app/interfaces/models';
import { Column, Entity } from 'typeorm';
import { CATEGORY_TABLE_SCHEMA } from '../schemas';
import { BaseEntity } from './common/BaseEntity';

@Entity(CATEGORY_TABLE_SCHEMA.TABLE_NAME)
export class CategoryEntity extends BaseEntity implements ICategoryModel {
    @Column({ name: CATEGORY_TABLE_SCHEMA.FIELDS.NAME, nullable: false })
    public name: string;

    @Column({ name: CATEGORY_TABLE_SCHEMA.FIELDS.IS_DELETED, nullable: false })
    public isDeleted: boolean;

    @Column({ name: CATEGORY_TABLE_SCHEMA.FIELDS.IS_ENABLE, nullable: false })
    public isEnable: boolean;
}
