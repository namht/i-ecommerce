import { BaseEntity as BaseEntityTypeOrm, Column, CreateDateColumn, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { BASE_TABLE_SCHEMA } from '../../schemas';
import { DateTransformer } from '../../transformers';

export abstract class BaseEntity extends BaseEntityTypeOrm {
    @PrimaryGeneratedColumn('uuid', {name: BASE_TABLE_SCHEMA.FIELDS.ID})
    id: string;

    @CreateDateColumn({name: BASE_TABLE_SCHEMA.FIELDS.CREATED_DATE, type: 'timestamptz', transformer: new DateTransformer(), update: false})
    createdDate: moment.Moment;

    @UpdateDateColumn({name: BASE_TABLE_SCHEMA.FIELDS.UPDATED_DATE, type: 'timestamptz', transformer: new DateTransformer(), update: false})
    updatedDate: moment.Moment;

    // @Column({name: BASE_TABLE_SCHEMA.FIELDS.VERSION, length: 50, nullable: true})
    // version: string;
}
