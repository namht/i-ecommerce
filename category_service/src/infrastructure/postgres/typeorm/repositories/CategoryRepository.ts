import { ICategoryRepository } from 'src/app/interfaces/repository';
import { Service } from 'typedi';
import { getRepository } from 'typeorm';
import { CategoryModel, CollectionWrap } from '../../../../app/models';
import { IOCServiceName } from '../../../../ioc/IocServiceName';
import { CategoryEntity } from '../entities';
import { CATEGORY_TABLE_SCHEMA } from '../schemas';
import BaseRepository from './BaseRepository';
import { CONNECTION_NAME } from '../connection';
import { IContext } from "../../../../app/interfaces/common/IContext";
@Service(IOCServiceName.CATEGORY_REPOSITORY)
export class CategoryRepository extends BaseRepository<CategoryEntity, CategoryModel> implements ICategoryRepository {
    protected readonly repository = getRepository(CategoryEntity, CONNECTION_NAME);
    protected readonly modelType = CategoryModel;
    protected readonly TABLE_SCHEMA = CATEGORY_TABLE_SCHEMA;
    public search(ctx: IContext, searchParams: any = {}, offset: number, limit: number, related): Promise<CollectionWrap<CategoryModel>> {
        let ids = searchParams.ids || null;
        limit = limit || 0;
        offset = offset || 0;
        let keyword = searchParams.key || null;
        let sortType = searchParams.sortType || "ASC";

        let query = (offset?: number, limit?: number, isOrderBy?: Boolean) => {
            return (q) => {
                q.where(`${CATEGORY_TABLE_SCHEMA.TABLE_NAME}.${CATEGORY_TABLE_SCHEMA.FIELDS.IS_DELETED} = :isDeleted`, { isDeleted: false });
                q.andWhere(`${CATEGORY_TABLE_SCHEMA.TABLE_NAME}.${CATEGORY_TABLE_SCHEMA.FIELDS.IS_ENABLE} = :isEnable`, { isEnable: true });
                if (ids != null) {
                    q.andWhere(`${CATEGORY_TABLE_SCHEMA.TABLE_NAME}.${CATEGORY_TABLE_SCHEMA.FIELDS.ID} IN (:...ids)`, { ids });
                }
                if (keyword != null) {
                    q.andWhere(`${CATEGORY_TABLE_SCHEMA.TABLE_NAME}.${CATEGORY_TABLE_SCHEMA.FIELDS.NAME} ILIKE :keyword`, { keyword: `%${keyword}%` });
                }

                if (offset != null) {
                    q.skip(offset);
                }
                if (limit != null) {
                    q.take(limit);
                }
                if (isOrderBy != null) {
                    q.orderBy(`${CATEGORY_TABLE_SCHEMA.TABLE_NAME}.${CATEGORY_TABLE_SCHEMA.FIELDS.CREATED_DATE}`, sortType);
                }
            };
        };
        return this.countAndQuery(query(), query(offset, limit, true));
    }
}
