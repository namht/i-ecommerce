import 'mocha';
import {CATEGORY, CATEGORY_BASE_URL } from '../contants';
const chai =  require('chai');
const chaiHttp = require('chai-http');
let should = chai.should();
chai.use(chaiHttp);


describe('categories', 
  () => {
    before(async function () {
        console.log("Before test");
    });

    after(function () {
        console.log("After test");
    });
    beforeEach(async () => {
        //Before each test we empty the database in your case
    });

    /*
    * GET LIST CATEGORY
    */
    describe('/GET categories', () => {
        it('it should GET list CATEGORY !!!', (done) => {
            chai.request(CATEGORY_BASE_URL)
                .get(CATEGORY)
                .end((err, res) => {
                  console.log(err);
                    should.exist(res.body);
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.data.should.be.a('array');
                    res.body.total.should.be.a('number');
                    done();
                });
        });
    });
});