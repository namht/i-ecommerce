# Node Core
The NodeJS framework is built with Clean Architecture, using NodeJS, Typescript, ExpressJS, TypeORM, PostgreSQL, Redis, etc... Easy to expand and maintain.

* Use data caching to improve performance. Cache into database or Redis.
* Use coding rules with ESLint.
* Build quickly with the generate module feature.
* Easy maintenance and expansion.
* Run & debug on .ts files by Visual Code.
* Unit test & coverage.
* Database migration.
* Demo table inheritance, refer to `Experiences` below of this guide and branch `feature/table-inheritance`.

### Patterns and Principles

- Clean architecture design pattern
- Domain driven design
- Repository pattern
- CQRS pattern
- Transfer object pattern
- Data mapper pattern
- Singleton pattern
- Factory pattern
- Dependency injection

### Technologies and Tools

- NodeJS
- Typescript
- ExpressJS
- TypeORM
- PostgreSQL
- Redis
- ESLint
- Mocha
- Visual Code

### Required

- NodeJS (version >= 12.9.0)
- Knowledge of Typescript, ES6, TypeORM, PostgreSQL.

### Document Related

- [Typescript](https://github.com/Microsoft/TypeScript#documentation)
- [ES6 - ECMAScript 2015](http://es6-features.org)
- [JavaScript Standard Style](https://standardjs.com/rules.html)
- [TypeORM](https://github.com/typeorm/typeorm) & [Migrations](https://github.com/typeorm/typeorm/blob/master/docs/migrations.md#migrations)
- [Routing controllers](https://github.com/typestack/routing-controllers#routing-controllers)

### Source Structure for 1 Service

```sh
- |-- .vscode ----------------------------------// Visual code configuration.
- |-- tests ------------------------------------// Testing folder.
- |-- build -------------------------------------// Built from the src directory.
- |-- node_modules
- |-- src --------------------------------------// Source of development.
- |------ configs
- |------------ env ----------------------------// Define environment folder.
- |---------------- configuration.yaml----------// Define environment variables from yaml file.
- |------------ index.ts -----------------------// Load config class.
- |------ libs
- |------ resources
- |------------ data ----------------------------// Initialize data.
- |------------ documents -----------------------// Document files (doc, docx, xls, xlsx, pdf,...).
- |------------ images --------------------------// Image files (jpg, jpeg, png, gif,...).
- |------ api
- |------------ controllers ---------------------// Navigate for requests.
- |------------ interceptors
- |------------ middlewares
- |------------------ Cors.ts ---// Cors.
- |------------------ Log.ts --------// Logs, track requests.
- |------------------ NotFound.ts ------// Capture 404 not found. 
- |------------------ Recover.ts -------
- |------------ index.ts -------------------// Initialize Api service and express config.
- |------ app
- |------------ constants
- |------------------ Common.ts
- |------------------ Database.ts
- |------------------ ErrorCode.ts
- |------------------ HttpCode.ts
- |------------------ index.ts
- |------------ interactors --------------------- // Business logic
- |------------------ http_clients -------------- // Call micro service (inter-service)
- |------------------------ CategoryClient.ts --- // Category client
- |------------------------ HttpClient.t s------- // Http client
- |------------------ ProductService.ts ------------------// Product services.
- |------------ interface ------------------------// All interface define
- |------------------- common
- |------------------- interactors
- |------------------- models
- |------------------- respository
- |------------------- index.ts
- |------------ model --------------------------- // Define model
- |------------------- common
- |------------------- BaseModel.ts
- |------------------- CategoryModel.ts
- |------------------- index.ts
- |------------------- ProductModel.ts
- |------ infrastructure 
- |------------ databases -----------------------// Data storage services.
- |------------------ redis ---------------------// In-memory database service.
- |------------------------ redis.ts
- |------------------ postgres ------------------// Database service.
- |------------------------ entities ------------// Define database structure.
- |------------------------ migrations ----------// Database migrations.
- |------------------------ repositories --------// Execution operations.
- |------------------------ schemas -------------// Define database schemas.
- |------------------------ transformers --------// Transform data before insert and after select from database.
- |------------------------ connection
- |----------------------------- index.ts ------ // connection init
- |------ ioc
- |------------ index.ts ----------------------- // Register IOC service dependency injection
- |-------------iocServiceName.ts -------------- // Register a name define dependency injection
- |------ server.ts -----------------------------// Main application.
- |-- .eslintignore -----------------------------// Eslint ignore.
- |-- .eslintrc.js ------------------------------// Eslint configuration.
- |-- .gitignore --------------------------------// Git ignore configuration.
- |-- ormconfig.ts ------------------------------// TypeORM configuration.
- |-- package.json
- |-- package-lock.json -------------------------// Lock package version.
- |-- README.md ---------------------------------// `IMPORTANT` to start the project.
- |-- tsconfig.json -----------------------------// Typescript configuration.
```

### NPM Commands

```s
npm run generate:module {param} -------------------// Generate module or sub-module: entity, schema, repository, usecase, controller,....
npm run generate:usecase {param} ------------------// Generate usecase for module or sub-module.
npm run cache:clear -------------------------------// Clear cache of TypeORM.
npm run migrate:generate {Migration_Name} ---------// Generate migration for update database structure.
npm run migrate:run -------------------------------// Run the next migrations for update database structure.
npm run migrate:revert ----------------------------// Revert migration for update database structure.
npm run lint
npm run build -------------------------------------// Build source before start with production environment.
npm test ------------------------------------------// Start unit test.
npm run dev ---------------------------------------// Start with development environment.
npm start -----------------------------------------// Start with production environment.
```


### Debug on Visual Code

* Press F5: build & start with debug mode.
* Debugging in .ts files.


## Quick Start 1 service (Examlple folder product_service)

> Please make sure PostgreSQL & Redis services is running. 
> Create two database icommerce-catetory and icommerce-product. 

Clone `src/config/env/configuration.yaml.sample` to `src/config/env/configuration.yaml` in the same directory and update configuration for project.
Install the npm package:

```
npm install
```

Run the migration for updating database structure (need to create database before):

```
npm run migrate:run
```

Run the below command for starting with development mode (or debug by visual code):

```
npm run dev
```

Also you can run test command and enjoy:

```
npm test
```

### Error Handler

We should define message error into `src\app\models\commom\Exception.ts`. Ex:

Usage:
```
import { Exception } from '../../app/models';
import { ErrorCode, HTTP_CODE } from '../../app/constants';
....
throw new Exception(
    ErrorCode.RESOURCE.MISSING_REQUIRED_FIELDS.CODE,
    ErrorCode.RESOURCE.MISSING_REQUIRED_FIELDS.MESSAGE,
    false,
    HTTP_CODE.BAD_REQUEST
);
```

> If you got error with status code 500, it's error system. Almost, this error is your source code, you need to find and fix it soon.

### Database Migration

- Database Migrations, a technique to help us keep our database changes under control. Database migration is the process of transforming data between various states without any human interaction. This process will allow us to track changes between schema updates.

- In a production environment, where data is already in the DB, we may have to migrate those as well. Same cases apply to testing and staging environments but production is a more fragile universe where mistakes are not forgiven. Say we need to split the Name field of our Users table into a First/Last Name fields combination. One approach would be to create a field called Last Name. Traverse the table, split the Name into two chunks and move the latter to the newly created field. Finally, rename the Name field into First Name. This is a case of data migrations.

- To generate new migration:
```
npm run migrate:generate Migration_Name
```
> Migration_Name should be named full meaning. Ex: Create_Table_Client, Add_Field_Email_In_Client, Modify_Field_Email_In_Client, Migrate_Old_Data_To_New_Data,....

- To run the next migrations for update database structure:
```
npm run migrate:run
```

- To revert back the previous migration:
```
npm run migrate:revert
```

### API Response Format

- Return error object [UnauthorizedError] with status code 401, this is handler of routing-controllers package.
```
Request:
curl -i -H Accept:application/json -X GET http://localhost:3000/api/v1/me

Response:
HTTP/1.1 401 Unauthorized
{
   "code": "ACCESS_DENIED_ERR",
   "message": "The token is required!"
}
```

- Return error object [SystemError] with status code 400, this is logic handler.
```
Request:
curl -i -H Accept:application/json -X POST http://localhost:3000/api/v1/auth/login -H Content-Type:application/json -d '{"email": "admin@localhost.com","password": "Nodecore@2"}'

Response:
HTTP/1.1 400 Bad Request
{
   "code": "DATA_INCORRECT_ERR",
   "message": "The email or password is incorrect!",
}
```

- Return data pagination with status code 200.
```
Request:
curl -i -H Accept:application/json -H 'Authorization:Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlSWQiOjEs...' -X GET http://localhost:3000/api/v1/roles

Response:
HTTP/1.1 200 OK
{
   "pagination": {
      "skip": 0,
      "limit": 10,
      "total": 2
   },
   "data": [
      {
         "id": ...,
         "name": ...
      },
      {
         "id": ...,
         "name": ...
      }
   ]
}
```

- Return data object with status code 200.
```
Request:
curl -i -H Accept:application/json -H 'Authorization:Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlSWQiOjEs...' -X POST http://localhost:3000/api/v1/users/dummy-clients -H Content-Type:application/json

Response:
HTTP/1.1 200 OK
{
	"data": {
      "total": 9,
      "successes": 0,
      "ignores": 9,
      "failures": 0,
      "failureIndexs": []
	}
}
```

- Return boolean data with status code 200.
```
Request:
curl -i -H Accept:application/json -H 'Authorization:Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlSWQiOjEs...' -X DELETE http://localhost:3000/api/v1/roles/{:id} -H Content-Type:application/json

Response:
HTTP/1.1 200 OK
{
   "data": true
}
```

### List Api Category Service

```
GET http://localhost:3006/products                       --> Find categories. options: &offset=&limit=&key=
POST http://localhost:3006/categories                      --> Create a category.
PUT http://localhost:3006/categories/:id                   --> Update a category.
GET http://localhost:3006/categories/:id                   --> Detail a category.
DELETE http://localhost:3006/categories/:id                --> Delete a category.
```

### List Api Product Service
```
GET http://localhost:3005/products                       --> Search products. options: &offset=&limit=&key=&branch=&fromPrice&toPrice=&color=
POST http://localhost:3005/products                      --> Create a category.
PUT http://localhost:3005/products/:id                   --> Update a category.
GET http://localhost:3005/products/:id                   --> Detail a category.
DELETE http://localhost:3005/products/:id                --> Delete a category.
```
